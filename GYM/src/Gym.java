import menus.Accueil;
import data.DataHandler;
import data.Service;
import java.time.*;

/** @author eq7 (Hadi, Alexandre, Alexis)
 *	Fonction main
 */
public class Gym {
	
	public static void main(String[] args) {	
		Accueil.handler();
	}
	
}

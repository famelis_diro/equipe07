package menus;

import data.DataHandler;

/** @author Hadi
 * S'occupe de service
 *
 */
public class OffreService extends Menu{
	public static void handler() {
	    String dateDeDebut;
	    String dateDeFin;
	    String heure;
	    int[] recurrence;
	    int capMax;
	    int profID;
	    double frais; // max 100.00$
	    String commentaires;

		
		System.out.println(longBar);
		System.out.println("Veuillez completez les champs suivants (sans crochet):");
		
		//Ci-dessous les champs necessaires pour creer un service
		dateDeDebut = getUserInput("-NoBar-Date de debut du service [JJ-MM-AAAA]");
		dateDeFin = getUserInput("-NoBar-Date de fin du service [JJ-MM-AAAA]");
		heure = getUserInput("-NoBar-Heure du service [HH:MM]");
		String recurrenceStr = getUserInput("-NoBar-Recurence hebdomadaire (1-lundi,2-mardi,3-mercredi...)\nPeut contenir plusieurs jours [1,3,7]");
		String[] recurrenceArray = recurrenceStr.split(",");
		recurrence = new int[recurrenceArray.length];
		//transforme lentree en une liste pour LocalDateTime
		for (int i = 0; i<recurrenceArray.length; i++) {
			recurrence[i]= Integer.parseInt(recurrenceArray[i]);
		}
		//repete tant que cest pas moins ou egal que 30 en capacite
		do {
			capMax = Integer.parseInt(getUserInput("-NoBar-Capacite maximale (Max 30) [nombre]"));
		}while(capMax>30);
		
		profID = Integer.parseInt(getUserInput("-NoBar-Numero du professionel (9 chiffres) [nombre]"));
		
		//repete tant que cest pas moins ou egal que 100 en frais
		do {
			frais = Double.parseDouble(getUserInput("-NoBar-Frais du service (max 100) [nombre]"));
		}while(frais>100);
		
		commentaires = getUserInput("-NoBar-Commentaires");

	    if(!confirmer("Confirmer la creation du service?")) {
			return;
		};
		
		aknowledge(DataHandler.ajoutDeService(dateDeDebut, dateDeFin, heure, recurrence, capMax, profID, frais, commentaires));

	}
}

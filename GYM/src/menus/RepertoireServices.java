package menus;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;

import data.DataHandler;
import data.Service;

/**
 * @author Hadi
 *	S'occupe des menu du repertoire des services. Guide l'agent a travers le processus d'inscription a sceance ou de confirmation de la presence a une seance.
 */
public class RepertoireServices extends Menu {
	
	public static void handler() {
		
		//Suite d'evenement pour afficher le repertoire de service, prendre choix de l'utilisateur, et l'inscrire ou confirmer sa presence a une seance.
		
		String listeServices = DataHandler.getServices();
		if (listeServices.equals("")) {
			listeServices += "Aucun services d'offert en ce moment";
			aknowledge(listeServices);
			return;
		}
		
		System.out.println(longBar);
		
		System.out.println(listeServices);
		//obtient le choix du service
		String entree = getUserInput("-NoBar-Veuillez choisir un code de service");
		
		//le transforme en int
		int codeService = Integer.parseInt(entree);
		//ligne de separation
		System.out.println(longBar);
		//obtient le service choisi
		Service serviceChoisi = DataHandler.getService(codeService);
		//valide que le service existe
		if(serviceChoisi.code.equals("invalide")) {
			aknowledge("Erreur: code de service non trouve");
			return;
		}
		System.out.print(serviceChoisi.listSeances());
		
		//obitent le choix de seance
		String choixSeanceStr = getUserInput("-NoBar-\nChoisissez la seance voulu a partir de la liste ci-dessus\nou appuyer 'N' pour annuler");
		
		
		if (choixSeanceStr.equals("N") || choixSeanceStr.equals("n")) {
			return;
		}
		int choixSeance = Integer.parseInt(choixSeanceStr);
		aknowledge(serviceChoisi.seanceDetail(choixSeance));
		String choixAgent = getUserInput("Appuyer sur 'r' pour confirmer la presence d'un client a la seance, \nAppuyer sur 'c' pour inscrire un client a la seance");

		//permet de choisir si on veut confirmer presence ou inscrire un client
		if(choixAgent.equals("r")||choixAgent.equals("R")) {
			int clientID = Integer.parseInt(getUserInput("Veuillez entrer votre numero unique"));
			
			Boolean verificationID = DataHandler.verifID(clientID)[0].equals("");
			Boolean verificationInscription = serviceChoisi.verifInscriptionIDSeance(choixSeance, clientID);
					
			if(verificationID) {
				aknowledge("Erreur! Numero unique invalide");
				return;
			}
			if(!verificationInscription) {
				aknowledge("Erreur! Client non-inscrit");
				return;
			}
			aknowledge("Valide! Vous pouvez participez a la seance.");
			return;
		}
		//Si l'utilisateur ne choisi aucune des deux options, retourne menu principale.
		if(!choixAgent.equals("c")&&!choixAgent.equals("C")) {
			return;
		}
		
		//prend et verifie le numero unique
		int clientID = Integer.parseInt(getUserInput("Veuillez entrer votre numero unique"));
		Boolean verificationID = DataHandler.verifID(clientID)[0].equals("");
		
		if(verificationID) {
			aknowledge("Erreur! Numero unique invalide");
			return;
		}
		
		String commentaireClient = getUserInput("Entrez un commentaire (facultatif)");
		
		//affiche message de confirmation
		String messageDeConfirmation = serviceChoisi.addInscription(choixSeance, clientID, commentaireClient);
		aknowledge(messageDeConfirmation);
		
		return;
	}
	
	
}

package menus;
import java.util.Scanner;

import data.DataHandler;

/**
 * @author Hadi
 *
 *	Classe qui s'occupe du menu Adhesion
 */
public class Adhesion extends Menu {

	/**
	 * Gere les options du menu actuelle ( chaque classe 'menu' � une methode handler() )
	 */
	public static void handler() {

		String[] options = {"Incription pour client","Incription pour professionel","Retour menu principal"};

		// L'affichage du menu se fait dans la classe Menu, qui prend une liste et en fait un menu
		int input = optionHandler(options);
		switch (input) {
		case 0: //incription client
		case 1: //incription prof.
			inscrire(input);
		case 2: //retour menu
			return;
		default:
			return;
		}
	}

	
	/** ajouter un utilisateur
	 * @param type : si c'est un professionel 1 ou un client 0
	 */
	public static void inscrire(int type) {

		//Prendre le nom de l'utilisateur
		String nomComplet = getUserInput("Entrez le nom complet de l'utilisateur");
		
		//client paye l'adhesion		
		if (type == 0) {
			//confirmation du paiement avec Y/N.
			Boolean paiementEffectuer = confirmer("Demander au client le montant pour frais d'adhesion.");
			//Si le paiment echoue:
			if (!paiementEffectuer) {
				System.out.println("\n Pas de paiement, l'incription a echouer.");
				return ;
			}
		}
		
		String messageDeBienvenue = DataHandler.ajoutUser(type, nomComplet);

		//acknoledge : Appuie sur ENTER pour continuer 
		aknowledge(messageDeBienvenue);

	}

}

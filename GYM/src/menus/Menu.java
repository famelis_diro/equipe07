package menus;

import java.util.Scanner;


/** @author Hadi
 * Cette classe regroupe la gestion des differents menus du programme
 */
abstract public class Menu {

	//facilite l'impression d'une bar de separation pour les classe heritant Menu.
	public static String longBar = "============================================\n";

	/**S'occupe d'afficher les options envoy�es en parametre a l'utilisateur et de recuperer son choix.
	 * @param options : Prend en parametre une array contenant les options a afficher a l'utilisateur
	 * @return l'option choisi par l'utilisateur
	 */
	public static int optionHandler(String[] options) {

		int chosen = 0;

		Scanner input = new Scanner(System.in);

		System.out.println(longBar);
		System.out.println("Veuillez choisir une des "+options.length+" options suivantes en entrant le chiffre appropri�:");

		for (int i = 0; i < options.length ; i++) {
			System.out.println(i+" - "+options[i]);
		}

		System.out.print("Votre choix ici: ");
		chosen = input.nextInt();

		System.out.println("Vous avez choisi '" + options[chosen]+"'");

		return chosen;

	}

	/** Methode qui Offre la possibilite de comfirmer ou refuser une situation (Y/N)
	 * @param message : message qui sera afficher
	 * @return Instruction de selection a l'utilisateur. 
	 */
	public static boolean confirmer(String message) {
		System.out.println(longBar);

		Scanner input = new Scanner(System.in);

		System.out.println(message);
		System.out.print("Appuyer 'Y' pour continuer ou 'N' pour annuler: ");
		char entree = input.next().charAt(0);
		if (entree == 'N' || entree=='n') {
			return false;
		}else if (entree == 'Y' || entree=='y') {
			return true;
		}
		//si la reponse est invalide, recursion.
		System.out.println("Reponse invalide");
		return confirmer(message);

	}
	

	/**methode qui pause le code et permet � l'agent de voir le dernier message. Doit appuyer sur un touche pour continuer.
	 * @param message qui sera affiche
	 */
	public static void aknowledge(String message) {
		System.out.println(longBar);

		Scanner input = new Scanner(System.in);
		if(!message.equals("")){
			System.out.println(message);
		}
		System.out.print("Appuyer ENTER pour continuer.");
		input.nextLine();
	}

	/** Permet d'obtenir le message en input par l'agent ou autre et le retourne en string
	 * @param message Sera affiche avec le prompt
	 * @return lentree utilisateur
	 */
	public static String getUserInput(String message) {
		if(!message.contains("-NoBar-")) {
			System.out.println(longBar);
		}else {
			message = message.replace("-NoBar-","");
		}
		Scanner input = new Scanner(System.in);
		System.out.print(message+": ");
		String entree = input.nextLine();
		return entree;
	}
	


}



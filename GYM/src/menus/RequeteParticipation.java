package menus;

import data.DataHandler;
import data.Professionel;

/**
 * @author Hadi
 * Classe qui permet a un professionel de consulter une liste des participants a ses seances.
 */
public class RequeteParticipation extends Menu{
	public static void handler() {
		//obtenir en input le num unique du professionel
		int profID = Integer.parseInt(getUserInput("Veuillez entrer le numero unique du professionel"));
		
		//Trouve le professionel en question
		Professionel professionnel = DataHandler.getProfFromID(profID);
		
		//verife la validite du code.
		if(professionnel == null) {
			aknowledge("Code invalide!");
			return;
		}
		
		String nombre = professionnel.participationSeances();
		//sil n'y a pas de participant, on l'indique.
		if(nombre.equals("")) {
			aknowledge("Aucun participant pour l'instant.");
			return;
		}
		//envoi la liste de participation
		aknowledge("Nombre de participant :\n" + professionnel.participationSeances());
		
		
	}
}

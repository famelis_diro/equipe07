package menus;
import data.DataHandler;

/**
 * @author eq7
 * 
 * Classe main qui initie les promptes du logiciel.
 */
public class Accueil extends Menu{

	/**
	 * Gere les options du menu actuelle ( chaque classe qui extend 'menu' � une methode handler() )
	 */ 
	public static void handler() {
		System.out.println("\n Bienvenue � gym!");
		
		while (true) {
			String[] options = {"Adh�sion gym","Viste gym","Repertoire de service","Offre de Service","Requete de participation"};
			//L'affichage du menu se fait dans la classe Menu, qui prend une liste et en fait un menu
			
			try {
			int entree = optionHandler(options);
			switch (entree) {
			case 0: //adhesion
				Adhesion.handler();
				continue;
			case 1: //Visite
				Visite.handler();
				continue;
			case 2: //repertoire de services
				RepertoireServices.handler();
				continue;
			case 3: //Offre de service
				OffreService.handler();
				continue;
			case 4: //requete de participation
				RequeteParticipation.handler();
				continue;
			default:
				continue;
			}
			}catch(Exception e) { //pour ne pas perdre les informations des listes en cas d'erreur
				aknowledge("Erreur d'entree => ("+e.getMessage()+")");
			}
			
		}
	}
}

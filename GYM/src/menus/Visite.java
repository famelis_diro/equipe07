package menus;
import java.util.Scanner;
import data.DataHandler;

/** @author Hadi
 * S'occupe de prendre le code du client et de valider son identit�
 *
 */
public class Visite extends Menu {
	
	public static void handler() {
		String message = verifIdent();
		aknowledge(message);

	}
	
	public static String verifIdent() {
		//obtient le num unique
		String stringID = getUserInput("Entrez le numero unique");
		int ID = Integer.parseInt(stringID);
		
		//le verifie et retourne sa validite
		String[] tempName = DataHandler.verifID(ID);
		//tempName est une liste qui contient le nom et le status du membre
		String name = tempName[0];
		String suspendue = tempName[1];
		
		if(name.equals("")){
			return "Numero invalide";
		}
		
		if(Integer.parseInt(suspendue)>0) {
			return "Membre suspendue, frais de "+suspendue+"$ a payer";
		}
		
		return "Valide: Bienvenue "+name+"!";
		
	}
	
}

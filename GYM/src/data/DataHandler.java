package data;
import java.io.*;
import java.util.ArrayList;

/** @author Hadi
 * Cette classe simule la base de donn�e pour le prototype
 */
public class DataHandler {
	
	//simule la base de donn�es Utilisateur, qui regroupe client et professionel
	public static ArrayList<Utilisateur> donneesClient = new ArrayList<Utilisateur>();
	public static ArrayList<Professionel> donneesProfessionel = new ArrayList<Professionel>();
	public static int nbDeService;
	
	//Ajoute un utilisateur en creant une nouvelle instance de client ou professionel
	public static String ajoutUser(int type, String nom) {
		if(type == 0) {
			donneesClient.add(new Client(nom));
			return donneesClient.get(donneesClient.size()-1).toString();
		} else {
			donneesProfessionel.add(new Professionel(nom));
		}
		return donneesProfessionel.get(donneesProfessionel.size()-1).toString();
		//retourne le message de bienvenue de l'utilisateur
		//a la classe incription, qui s'occupe de l'afficher
		
	}

	//verifie que l'ID n'as jamais ete utilise
	public static boolean newID(int ID) {
		for(int i = 0; i<donneesClient.size(); i++) {
			if(donneesClient.get(i).uniqueID == ID) {
				return true;
			}
		}
		return false;
	}
	
	public static String[] verifID(int ID) {
		
		String[] userInfo = {"",""+true};
		
		for(int i = 0; i<donneesClient.size(); i++) {
			
			Utilisateur tempUser = donneesClient.get(i);
			if(donneesClient.get(i).uniqueID == ID) {
				userInfo[0] =tempUser.fullName;
				userInfo[1] = ""+tempUser.suspendue;
				return userInfo;
			}
		}
		return userInfo;
	}
	
	public static Professionel getProfFromID(int ID) {
		for(int i = 0; i<donneesProfessionel.size(); i++) {
			
			if(donneesProfessionel.get(i).uniqueID==ID) {
				return donneesProfessionel.get(i);
			}
			
		}
		return null;
	}
	
	
	public static String getServices() {
		String listeServices = "";
		
		//passe par tout les services des professionel
		for(int i = 0; i<donneesProfessionel.size(); i++) {
			Professionel professionel = donneesProfessionel.get(i);
			String sommaireServices = professionel.listeServices();
			if(!sommaireServices.equals("")) {
				listeServices += sommaireServices +"\n";
			}
		}
		
		//retourne le texte qui contient ces 
		return listeServices;
		
	}
	
	public static Service getService(int codeService) {
		Service serviceTemp = new Service();
		for(int i = 0; i<donneesProfessionel.size(); i++) {
			Professionel profActuel = donneesProfessionel.get(i);
			serviceTemp = profActuel.findService(codeService);
			if(serviceTemp.codeEquals(codeService)) {
				return serviceTemp;
			}
			
		}
		return new Service();
	}

	public static String ajoutDeService(String dateDeDebut, String dateDeFin, String heure, int[] recurrence, int capMax, int profID, double frais, String commentaires) {
		Professionel profChoisi = getProfFromID(profID);
		if(profChoisi == null) {
			return "erreur, professionel non-trouve!";
		}
		nbDeService+=1;
		int code = nbDeService;
		return profChoisi.ajoutDeService(dateDeDebut, dateDeFin, heure, recurrence, capMax, profID, code, frais, commentaires);
	}
	
	
	
}

package data;

import java.util.ArrayList;
import java.time.*;
import java.time.format.DateTimeFormatter;

public class Service {
	
    private ArrayList<Seance> seancesDuService = new ArrayList<Seance>();
    
    private LocalDateTime dateEtHeureCreation;
    private LocalDate dateDeDebut;
    private LocalDate dateDeFin;
    private LocalTime heure;
    private ArrayList<DayOfWeek> recurrence = new ArrayList<DayOfWeek>();
    private int capMax;
    private int profID;
    public String code;
    private double frais; // max 100.00$
    private String commentaires;

    /**
     * @param dateDeDebut Date de debut du service
     * @param dateDeFin Date de fin du service
     * @param heure Heure a laquelle le service est donne
     * @param recurrence Jours de la semaine auxquels le service est donne
     * @param capMax Capacite maximale du service (max 30)
     * @param profID Numero unique du professionnel offrant le service (max 9 chiffres)
     * @param code Code unique du service (max 7 chiffres)
     * @param frais Frais du professionnel pour ce service (max 100$)
     * @param commentaires Commentaires du professionnel pour ce service
     */
    public Service(String dateDeDebut, String dateDeFin, String heure, int[] recurrence, int capMax, int profID, int code, double frais, String commentaires) {
    	this.dateEtHeureCreation = LocalDateTime.now();
        this.dateDeDebut = toDate(dateDeDebut);
        this.dateDeFin = toDate(dateDeFin);
        this.heure = LocalTime.parse(heure);
        
        for (int r: recurrence) {
        	this.recurrence.add(DayOfWeek.of(r));
        }
        
        this.capMax = capMax;
        this.profID = profID;
        this.code = codeToString(code);
        this.frais = frais;
        this.commentaires = commentaires;
        
        LocalDate date = this.dateDeDebut;
        while (!date.equals(this.dateDeFin)) {
        	for (int i=0; i < this.recurrence.size(); i++) {
            	if (date.getDayOfWeek() == this.recurrence.get(i)) {
            		this.seancesDuService.add(new Seance(date));
            	}
            }
        	date = date.plusDays(1);
        }
    }
    
    /**
     * @return Heure en String de format "hh:mm"
     */
    public String getHeure() {
    	return this.heure.format(DateTimeFormatter.ofPattern("HH:mm"));
    }
    
    public Service() {
    	this.code = "0";
    }
    
    /**
     * @return Une ArrayList des Seance du service
     */
    public ArrayList<Seance> seancesDuService(){
    	return this.seancesDuService;
    }
    
    /**
     * @param code Code de service a convertir
     * @return Meme code en String (ajoute des zeros au debut pour qu'il soit exactement 7 chiffres de longueur
     */
    public String codeToString(int code) {
    	String codeString = "";
    	
    	for (int i=0; i < (7 - ("" + code).length()); i++) {
    		codeString = codeString + "0";
        }
    	codeString = codeString + code;
    	
    	return codeString;
    }
    
    /**
     * @param choixClient Choix de la seance auquelle le client voudrait s'incrire
     * @param ID Numero unique du client (9 chiffres max)
     * @return lance la verification du numero du client
     */
    public Boolean verifInscriptionIDSeance(int choixClient, int ID) {
    	Seance seance = seancesDuService.get(choixClient);
    	return seance.verifInscritionID(ID);
    }
    
    /**
     * @param choixClient Choix de la seance auquelle le client voudrait s'incrire
     * @param clientID Numero unique du client (9 chiffres max)
     * @param commentaireClient Commentaire (facultatif) du client
     * @return Ajoute l'inscription a la liste de la seance
     */
    public String addInscription(int choixClient, int clientID, String commentaireClient) {
    	Seance seance = seancesDuService.get(choixClient);
    	
    	return seance.addInscription(this.profID, clientID, this.code, commentaireClient)+"\nLe montant a payer maintenant est de "+frais+"$.";
    }

    /**
     * @param dateEnTexte Date en String de format "jj-mm-aaaa"
     * @return La meme date en LocalDate
     */
    public static LocalDate toDate(String dateEnTexte) {

        String[] tempArray = dateEnTexte.split("-");

        int[] dateEnInt = {Integer.parseInt(tempArray[0]),
                Integer.parseInt(tempArray[1]),
                Integer.parseInt(tempArray[2])};
        return LocalDate.of(dateEnInt[2],dateEnInt[1],dateEnInt[0]);

    }
    
    
    /**
     * @return Retourne une liste des seances disponibles pour un service
     */
    public String listSeances() {
    	String list = "List des seances:\n";
    	
    	int i = 0;
    	for (Seance s: this.seancesDuService) {
    		list = list + i + " - " + s.toString() + " a " + this.heure.format(DateTimeFormatter.ofPattern("HH:mm")) + "\n";
    		i++;
    	}
    	
    	return list;
    }
    
    
    /**
     * @return Retourne un sommaire succint de ce service
     */
    public String sommaire() {
    	String s = "[" + this.code + "] Du " + this.dateDeDebut.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    	
    	s = s + " au " + this.dateDeFin.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    	s = s + " a " + this.heure.format(DateTimeFormatter.ofPattern("HH:mm"));
    	s = s + ", offert ";
    	
    	for (DayOfWeek d: this.recurrence) {
    		s = s + d + " ";
    	}
    	if(commentaires.length()>10) {
    		s = s + ": " + this.commentaires.substring(0,12) + "...";
    	}
    	return s;
    }
    
    /**
     * @param codeEntree Le code de service qu'on veut comparer au code de ce service
     * @return Retourne TRUE si le code est le meme
     */
    public boolean codeEquals(int codeEntree) { 
    	int codeInt = Integer.parseInt(this.code);
    	return codeInt == codeEntree;
    }
   
    public String toString() {
    	String s = "";
    	
    	s = s + "Code du service: " + this.code + "\n";
    	s = s + "Date et heure de creation: " + this.dateEtHeureCreation.format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")) + "\n";
    	s = s + "Date de debut: " + this.dateDeDebut.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + "\n";
    	s = s + "Date de fin: " + this.dateDeFin.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + "\n";
    	s = s + "Heure: " + this.heure.format(DateTimeFormatter.ofPattern("HH:mm")) + "\n";
    	s = s + "Les jours suivants : ";
    	
    	for (DayOfWeek d: this.recurrence) {
    		s = s + d + " ";
    	}
    	s = s + "\n";

    	s = s + "Capacite maximale: " + this.capMax + "\n";
    	s = s + "Frais: " + this.frais + "$\n";
    	s = s + "Commentaires: " + this.commentaires;
    	
    	return s;
    }
    
    //
    /**
     * @param choixClient Numero de la seance auquelle le client voudrait s'inscrire
     * @return impression detaillee de seance
     */
    public String seanceDetail(int choixClient) {
    	Seance seanceChoisi = seancesDuService.get(choixClient);
    	
    	
    	return seanceChoisi.showDetails(frais, profID, code)+ "Commentaires: " +this.commentaires+"\n";
    }
}


package data;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Classe qui represente une de chacune Seances possible selon la date de debut et de fin du service (et des jours de la semaine de recurrence)
 *
 */
public class Seance {
	private LocalDate dateSeance;
	private ArrayList<InscriptionSeance> listeInscriptions;
	
	
	
	
	public Seance(LocalDate dateSeance) {
		this.dateSeance = dateSeance;
		this.listeInscriptions = new ArrayList<InscriptionSeance>();
	}
	
	public String toString() {
		return "Seance du " + this.dateSeance.getDayOfWeek() + " " + this.dateSeance.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
	}
	
	public int nombreInscription() {
		return this.listeInscriptions.size();
	}
	
	
	/** Montre la seance en detail
	 * @return un string des details
	 */
	public String showDetails(double frais, int profID, String codeService) {
		String result = "";
		
		result = result + "Date a  laquelle le service sera fourni: " + this.dateSeance.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + "\n";
		result = result + "Numero du professionnel: " + profID + "\n";
		result = result + "Code du service de cette seance: " + codeService + "\n";
		result = result + "Prix: " + frais + "$\n";
		
		return result;
	}
	
	/**Permet l'ajout d'une incription d'un client
	 */
	public String addInscription(int numProf, int numMembre, String codeDuService, String commentaires) {
		InscriptionSeance newIncription = new InscriptionSeance(this.dateSeance, numProf, numMembre, codeDuService, commentaires);
		this.listeInscriptions.add(newIncription);
		
		return newIncription.toString();
	}
	
	
	/** pour confirmer la presence d'un client
	 * @param ID du client
	 * @return si le client est vraiement inscrie a la seance
	 */
	public Boolean verifInscritionID(int ID) {
		for (int i = 0 ; i<listeInscriptions.size(); i++) {
			InscriptionSeance inscription = this.listeInscriptions.get(i);
			if(inscription.numMembre == ID) {
				return true;
			}
		}
		
		return false;
	}
	
}

package data;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class InscriptionSeance {
	// Attributs
	public LocalDateTime dateEtHeureInscription;
	public LocalDate dateService;
	public int numProf;
	public int numMembre;
	public String codeDuService;
	public String commentaires;
	
	/**
	 * @param dateService Date à laquelle le service sera fourni.
	 * @param numProf Numéro du professionnel offrant le service.
	 * @param numMembre Numéro du membre qui s'inscrit.
	 * @param codeDuService Code du service.
	 * @param commentaires Commentaires (facultatifs) du membre.
	 */
	public InscriptionSeance(LocalDate dateService, int numProf, int numMembre, String codeDuService, String commentaires) {
		this.dateEtHeureInscription = LocalDateTime.now();
		this.dateService = dateService;
		this.numProf = numProf;
		this.numMembre = numMembre;
		this.codeDuService = codeDuService;
		this.commentaires = commentaires;
	}
	
	/**
	 * @param dateEnTexte Date en format JJ-MM-AAAA
	 * @return Date en type LocalDate
	 */
	public static LocalDate toDate(String dateEnTexte) {
        String[] tempArray = dateEnTexte.split(dateEnTexte);

        int[] dateEnInt = {Integer.parseInt(tempArray[0]),
                Integer.parseInt(tempArray[1]),
                Integer.parseInt(tempArray[2])};
        return LocalDate.of(dateEnInt[2],dateEnInt[1],dateEnInt[0]);
    }
	

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String result = "";
		
		result = result + "Date et heure de l'inscription: " + this.dateEtHeureInscription.format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")) + "\n";
		result = result + "Date a� laquelle le service sera fourni: " + this.dateService.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) + "\n";
		result = result + "Numero du professionnel: " + this.numProf + "\n";
		result = result + "Numero du membre: " + this.numMembre + "\n";
		result = result + "Code du service: " + this.codeDuService + "\n";
		result = result + "Commentaires: " + this.commentaires + "\n";
		
		return result;
	}
}

package data;

public abstract class Utilisateur {
	
	protected String fullName;
	protected int uniqueID;
	protected String type;
	protected int suspendue;
	

	public Utilisateur(String nom) {
			this.uniqueID = randID();
			this.fullName = nom;
			this.suspendue = 0;
	}
	
	//Genere un nombre unique a 9 chiffres
	public static int randID() {
		
		int randNumber;
		do {
		randNumber = (int) Math.round(Math.random() * 900000000 +100000000);
		} while (DataHandler.newID(randNumber));
		
		return randNumber;
	}
	
	public String toString() {
		return  "Bienvenue, "+fullName+"! \n"
				+ "Voici votre code d'acces unique: "
				+uniqueID+". \nVous etes un: "+type+".";
		
	}
	
}

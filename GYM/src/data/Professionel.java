package data;
import java.util.ArrayList;

/**
 * @author Hadi
 *	Classe du professionel qui store les services de ce dernier. 
 *
 */
public class Professionel extends Utilisateur {

	private ArrayList<Service> servicesDuProf = new ArrayList<Service>();
	//constructeur de professionel, utilise le constructeur de Utilisateur et specifie le type.
	public Professionel(String nom) {
		super(nom);
		this.type = "professionel";
	}
	//retourne les informations concernant la participations aux seances.
	public String participationSeances() {
		String participation = "";
		if(servicesDuProf.size()==0) {
			return "";
		}
		for(int i = 0; i < this.servicesDuProf.size();i++) {
			Service serviceActuelle = servicesDuProf.get(i);
			ArrayList<Seance> seances = serviceActuelle.seancesDuService();
			for (int j = 0; j < seances.size(); j++) {
				Seance tempSeance = seances.get(j);
				participation +="Il y a "+ tempSeance.nombreInscription()+" participant(s) dans la "+tempSeance.toString()+" a "+serviceActuelle.getHeure()+"\n";
				
			}
		}
		return participation;
	}
	
	
	//Ajoute un service a la arraylist
	public String ajoutDeService(String dateDeDebut, String dateDeFin, String heure, int[] recurrence, int capMax, int profID, int code, double frais, String commentaires) {
		Service nouveauService = new Service(dateDeDebut, dateDeFin, heure, recurrence, capMax, profID, code, frais, commentaires);
		servicesDuProf.add(nouveauService);
		return nouveauService.toString();
	}

	//Affiche une liste sommaire des services (sera utilise pour repertoire de service)
	public String listeServices() {
		if(servicesDuProf.size() == 0) {
			return "";
		}
		String serviceDuProfessionel = "Service offerts par: "+this.fullName+"\n";
		for(int i = 0; i<servicesDuProf.size(); i++) {
			Service serviceActuelle = servicesDuProf.get(i);
			serviceDuProfessionel += serviceActuelle.sommaire()+"\n";

		}

		return serviceDuProfessionel;
	}

	//Permet de trouver un service. Si il n'est pas trouvee, retourne code 0 (non-existant)
	public Service findService(int codeService) {
		Service service = new Service();
		for (int i = 0; i<servicesDuProf.size();i++) {
			service = servicesDuProf.get(i);
			if(service.codeEquals(codeService)) {
				return service;
			}
		}
		return service;
	}
	
	

}

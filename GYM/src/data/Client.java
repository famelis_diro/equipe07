package data;
/**
 * classe du client qui contient son type
 * 
 */
public class Client extends Utilisateur {
	public Client(String nom) {
		super(nom);
		this.type = "client";
	}
}
